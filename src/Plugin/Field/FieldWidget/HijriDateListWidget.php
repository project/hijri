<?php

namespace Drupal\hijri\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDatelistWidget;

/**
 * Plugin implementation of the 'HijriDateListWidget' widget.
 *
 * @FieldWidget(
 *   id = "HijriDateListWidget",
 *   label = @Translation("Hijri date list widget"),
 *   field_types = {
 *     "Hijri"
 *   }
 * )
 */
class HijriDateListWidget extends DateTimeDatelistWidget {

  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   *
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    // @todo We may have to make this range configurable from settings form.
    // $element['value']['#date_year_range'] = '1300:1500';
    // $element['value']['#date_year_range'] = '-3:+3';
    $element['value']['#element_validate'] = [
      [$this, 'validate'],
    ];

    return $element;
  }

  /**
   * Validate field.
   */
  public function validate(&$element, FormStateInterface &$form_state, $form) {
    // parent::validate($element, $form_state, $form);.
    // @todo Fix this issue to support more than one field.
    // @todo Get field name the right way.
    $field_name = $element['#array_parents'][0];
    $field_values = $form_state->getValue([$field_name]);

    $value = $field_values[0]['value'];

    // @todo We have to validate the provided Hijri date.
    if ($element['#required'] || (strlen($value) > 0) || (strlen($value) > 0)) {
      $form_state->setError($element, $this->t('Field is required.'));
    }
  }

}
