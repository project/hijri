<?php

namespace Drupal\hijri\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'datetime' formatter as Hijri.
 *
 * @FieldFormatter(
 *   id = "hijri_field_formatter",
 *   label = @Translation("Hijri"),
 *   field_types = {
 *     "datetime",
 *   }
 * )
 */
class HijriFieldFormatter extends FormatterBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a TimestampAgoFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param mixed $hijri_formatter
   *   Hijri formatter.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DateFormatterInterface $date_formatter, $hijri_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->dateFormatter = $date_formatter;
    $this->hijri_formatter = $hijri_formatter;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('hijri.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'hijri_formatter' => "long",
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $hijri_types = self::hijriTypes();
    $form['hijri_formatter'] = [
      '#type' => 'select',
      '#title' => $this->t('Hijri'),
      '#default_value' => $this->getSetting('hijri_formatter'),
      '#options' => $hijri_types,
      '#description' => $this->t('Select the display you want to be in the view page'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $hijri_types = self::hijriTypes();
    $summary[] = $hijri_types[$this->getSetting('hijri_formatter')];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];

    foreach ($items as $delta => $item) {
      if ($item->value) {
        $hijri_types = [
          'full' => $this->t('Hijri full format: @hijri on @gregorian', [
            '@hijri' => $this->hijri_formatter->format(strtotime($item->value), 'custom', 'l j F Y'),
            '@gregorian' => $this->dateFormatter->format(strtotime($item->value), 'custom', 'F j, Y'),
          ]),
          'long' => $this->t('@date', ['@date' => $this->hijri_formatter->format(strtotime($item->value), 'long', NULL)]),
          'medium' => $this->t('@date', ['@date' => $this->hijri_formatter->format(strtotime($item->value), 'medium', NULL)]),
          'short' => $this->t('@date', ['@date' => $this->hijri_formatter->format(strtotime($item->value), 'short', NULL)]),
        ];

        $elements[$delta] = [
          '#cache' => [
            'contexts' => [
              'timezone',
            ],
          ],
          '#markup' => $hijri_types[$this->getSetting('hijri_formatter')],
        ];
      }

    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function hijriTypes() {

    $hijri_types = [
      'full' => $this->t('Hijri full format: @hijri on @gregorian', [
        '@hijri' => $this->hijri_formatter->format(time(), 'custom', 'l j F Y'),
        '@gregorian' => $this->dateFormatter->format(time(), 'custom', 'F j, Y'),
      ]),
      'long' => $this->t('Hijri long format: @date', ['@date' => $this->hijri_formatter->format(time(), 'long', NULL)]),
      'medium' => $this->t('Hijri medium format: @date', ['@date' => $this->hijri_formatter->format(time(), 'medium', NULL)]),
      'short' => $this->t('Hijri short format: @date', ['@date' => $this->hijri_formatter->format(time(), 'short', NULL)]),
    ];
    return $hijri_types;
  }

}
