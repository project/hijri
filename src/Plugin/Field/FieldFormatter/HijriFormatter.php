<?php

namespace Drupal\hijri\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'HijriDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "HijriDefaultFormatter",
 *   label = @Translation("Hijri"),
 *   field_types = {
 *     "Hijri"
 *   }
 * )
 */
class HijriFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->value . ', ' . $item->correction,
      ];
    }

    return $elements;
  }

}
