<?php

namespace Drupal\hijri\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'hijri' field type.
 *
 * @FieldType(
 *   id = "Hijri",
 *   label = @Translation("Hijri"),
 *   description = @Translation("Hijri"),
 *   default_widget = "HijriDefaultWidget",
 *   default_formatter = "HijriDefaultFormatter"
 * )
 */
class HijriItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date value'))
      ->setRequired(TRUE);

    $properties['correction'] = DataDefinition::create('string')
      ->setLabel(t('Correction'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'The date value.',
          'type' => 'varchar',
          'length' => 20,
        ],
        'correction' => [
          'description' => 'The correction value.',
          'type' => 'varchar',
          'length' => 20,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}
