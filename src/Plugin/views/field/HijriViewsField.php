<?php

namespace Drupal\hijri\Plugin\views\field;

use Drupal\views\Plugin\views\field\Date;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * A handler to provide custom displays for Hijri dates.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("hijri_views_field")
 */
class HijriViewsField extends Date {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $hijri_formatter = \Drupal::service('hijri.formatter');

    $form['date_format']['#options']['hijri_full'] = t('Hijri full format: @hijri on @gregorian', [
      '@hijri' => $hijri_formatter->format(time(), 'custom', 'l j F Y'),
      '@gregorian' => \Drupal::service('date.formatter')->format(time(), 'custom', 'F j, Y'),
    ]);
    $form['date_format']['#options']['hijri_long'] = $this->t('Hijri long format: @date', ['@date' => $hijri_formatter->format(time(), 'long', NULL)]);
    $form['date_format']['#options']['hijri_medium'] = $this->t('Hijri medium format: @date', ['@date' => $hijri_formatter->format(time(), 'medium', NULL)]);
    $form['date_format']['#options']['hijri_short'] = $this->t('Hijri short format: @date', ['@date' => $hijri_formatter->format(time(), 'short', NULL)]);
    $form['date_format']['#options']['hijri_custom'] = $this->t('Hijri custom');

    $form['custom_date_format']['#states']['visible'][] = [
      ':input[name="options[date_format]"]' => ['value' => 'hijri_custom'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $list = [
      'hijri_long',
      'hijri_medium',
      'hijri_short',
    ];
    $value = $this->getValue($values);
    $hijri_formatter = \Drupal::service('hijri.formatter');
    $format = $this->options['date_format'];
    if ($format == 'hijri_full') {
      return t('@hijri on @gregorian', [
        '@hijri' => $hijri_formatter->format($value, 'custom', 'l j F Y'),
        '@gregorian' => \Drupal::service('date.formatter')->format($value, 'custom', 'F j, Y'),
      ]);
    }
    elseif (in_array($format, $list)) {
      $explode = explode("_", $format);
      return $hijri_formatter->format($value, $explode[1]);
    }
    elseif ($format == 'hijri_custom') {
      $custom_format = $this->options['custom_date_format'];
      return $hijri_formatter->format($value, $format, $custom_format);
    }

    return parent::render($values);
  }

}
