<?php

namespace Drupal\hijri\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to render the Hijri date.
 *
 * @Block(
 *   id = "hijri_date_block",
 *   admin_label = @Translation("Hijri Date"),
 * )
 */
class HijriDate extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\hijri\HijriFormatter $hijri_formatter */
    $hijri_display = \Drupal::config('hijri.config')->get('hijri_display_block');
    $hijri_formatter = \Drupal::service('hijri.formatter');

    switch ($hijri_display) {
      case 'full':
        $format = $this->t('@hijri on @gregorian', [
          '@hijri' => $hijri_formatter->format(time(), 'custom', 'l j F  Y'),
          '@gregorian' => \Drupal::service('date.formatter')->format(time(), 'custom', 'F j, Y'),
        ]);
        break;

      case 'long':
      case 'medium':
      case 'short':
        $format = $hijri_formatter->format(time(), $hijri_display);
        break;

      default:
        $format = $hijri_formatter->format(time());
        break;
    }

    return [
      '#markup' => $format,
    ];
  }

}
