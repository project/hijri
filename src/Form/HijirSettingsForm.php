<?php

namespace Drupal\hijri\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * {@inheritdoc}
 */
class HijirSettingsForm extends ConfigFormBase {
  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, DateFormatterInterface $date_formatter, $hijri_formatter) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->dateFormatter = $date_formatter;
    $this->hijri_formatter = $hijri_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('date.formatter'),
      $container->get('hijri.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hijri_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hijri.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $system_date = $this->config('hijri.config');
    $correction = $system_date->get('correction_value');
    /** @var \Drupal\hijri\HijriFormatter $hijri_formatter */

    $hijri_types = [
      'full' => $this->t('Hijri full format: @hijri on @gregorian', [
        '@hijri' => $this->hijri_formatter->format(time(), 'custom', 'l j F Y'),
        '@gregorian' => $this->dateFormatter->format(time(), 'custom', 'F j, Y'),
      ]),
      'long' => $this->t('Hijri long format: @date', ['@date' => $this->hijri_formatter->format(time(), 'long', NULL)]),
      'medium' => $this->t('Hijri medium format: @date', ['@date' => $this->hijri_formatter->format(time(), 'medium', NULL)]),
      'short' => $this->t('Hijri short format: @date', ['@date' => $this->hijri_formatter->format(time(), 'short', NULL)]),
      'none' => $this->t('None'),
    ];
    // Content types settings.
    $form['hijri_settings'] = [
      '#type' => 'vertical_tabs',
    ];
    $form['hijri_settings']['correction'] = [
      '#type' => 'details',
      '#title' => $this->t('Hijri Correction'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'hijri_settings',
      '#weight' => 0,
    ];
    $form['hijri_settings']['correction']['hijri_correction_value'] = [
      '#type' => 'select',
      '#title' => $this->t('Correction days'),
      '#options' => [
        -2 => -2,
        -1 => -1,
        0 => 0,
        +1 => +1,
        +2 => +2,
      ],
      '#default_value' => $correction,
    ];

    // Per-path visibility.
    $form['hijri_settings']['node_type'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'hijri_settings',
      '#weight' => 0,
    ];
    $types = array_map(['\Drupal\Component\Utility\Html', 'escape'], node_type_get_names());
    $form['hijri_settings']['node_type']['hijri_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Hijri date Correction field for specific content types'),
      '#default_value' => $system_date->get('hijri_types'),
      '#options' => $types,
      '#description' => $this->t('Add/Remove the Correction field for content type.'),
      '#multiple' => TRUE,
    ];

    $form['hijri_settings']['node_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Node Display'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'hijri_settings',
      '#weight' => 0,
    ];

    $form['hijri_settings']['node_display']['hijri_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Hijri date on the node view'),
      '#default_value' => $system_date->get('hijri_display'),
      '#options' => $hijri_types,
      '#description' => $this->t('Select the display type you want to be in the node view page'),
    ];

    $form['hijri_settings']['hijri_comment_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Comment Display'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'hijri_settings',
      '#weight' => 0,
    ];

    $form['hijri_settings']['hijri_comment_display']['hijri_comment_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Hijri date on the comment area'),
      '#default_value' => $system_date->get('hijri_comment_display'),
      '#options' => $hijri_types,
      '#description' => $this->t('Select the display type you want to be in the comment area'),
    ];

    $form['hijri_settings']['block_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Block Display'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'hijri_settings',
      '#weight' => 0,
    ];

    // Unset none display for block.
    unset($hijri_types['none']);

    $form['hijri_settings']['block_display']['hijri_display_block'] = [
      '#type' => 'radios',
      '#title' => $this->t('Hijri date on Hijri block'),
      '#default_value' => $system_date->get('hijri_display_block'),
      '#options' => $hijri_types,
      '#description' => $this->t('Select the display type you want in Hijri block'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('hijri.config')
      ->set('correction_value', $form_state->getValue('hijri_correction_value'))
      ->set('hijri_types', $form_state->getValue('hijri_types'))
      ->set('hijri_display', $form_state->getValue('hijri_display'))
      ->set('hijri_comment_display', $form_state->getValue('hijri_comment_display'))
      ->set('hijri_display_block', $form_state->getValue('hijri_display_block'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
