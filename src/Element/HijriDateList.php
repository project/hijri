<?php

namespace Drupal\hijri\Element;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Element\Datelist;
use Drupal\Core\Form\FormStateInterface;

/**
 * Override the datelist element.
 *
 * @FormElement("datelist")
 */
class HijriDateList extends Datelist {

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // If the input is defined.
    if ($input !== FALSE) {
      if (isset($input['year']) && ($input['year'] > 0)) {
        // The year value has been set.
        $return = $input;
        $date = date('c');
        $return['object'] = $date;
        // $form_state->setError($element, t('Test error.'));
        return $return;
      }
    }
    else {
      // This will be called when prepare the input for form.
    }
    return $input;
  }

  /**
   * {@inheritdoc}
   */
  public static function processDatelist(&$element, FormStateInterface $form_state, &$complete_form) {
    // The value callback has populated the #value array.
    $date = !empty($element['#value']['object']) ? $element['#value']['object'] : NULL;

    // Set a fallback timezone.
    if ($date instanceof DrupalDateTime) {
      $element['#date_timezone'] = $date->getTimezone()->getName();
    }
    elseif (!empty($element['#timezone'])) {
      // We will do nothing.
    }
    else {
      $element['#date_timezone'] = date_default_timezone_get();
    }

    // Load the date helper to prepare some needful arrays.
    $date_helper = new DateHelper();
    $element['#tree'] = TRUE;

    // Determine the order of the date elements.
    $order = !empty($element['#date_part_order']) ? $element['#date_part_order'] : [
      'year',
      'month',
      'day',
    ];
    $text_parts = !empty($element['#date_text_parts']) ? $element['#date_text_parts'] : [];

    // Output multi-selector for date.
    foreach ($order as $part) {
      switch ($part) {
        case 'day':
          // Generating the days array. We will assume September since September
          // is only 30 days.
          $options = $date_helper->days($element['#required'], 9, 2000);
          $format = 'j';
          $title = t('Day');
          break;

        case 'month':
          /**
           *
         *
* @var \Drupal\hijri\HijriFormatter $hijri
*/
          $hijri = \Drupal::service('hijri.formatter');
          $options = $hijri->hijriMonthNames();
          $format = 'n';
          $title = t('Month');
          break;

        case 'year':
          // @todo User should be able to set year range in field settings.
          /**
           *
         *
* @var \Drupal\hijri\HijriFormatter $hijri
*/
          $hijri = \Drupal::service('hijri.formatter');
          // @todo Support year range instead of current year.
          $current_year = $hijri->format(time(), 'custom', 'Y');
          $format = 'n';
          $title = t('Year');

          $min = '';
          $max = '';
          $range = range(
                empty($min) ? intval($current_year - 3) : $min,
                empty($max) ? intval($current_year + 3) : $max
            );
          // Making the keys equal values.
          $range = array_combine($range, $range);
          // We may allow for an empty option if applicable.
          $options = $range;
          break;

        default:
          $format = '';
          $options = [];
          $title = '';
      }

      $default = isset($element['#value'][$part]) && trim($element['#value'][$part]) != '' ? $element['#value'][$part] : '';
      $value = $date instanceof DrupalDateTime && !$date->hasErrors() ? $date->format($format) : $default;
      if (!empty($value) && $part != 'ampm') {
        $value = intval($value);
      }

      $element['#attributes']['title'] = $title;
      $element[$part] = [
        '#type' => in_array($part, $text_parts) ? 'textfield' : 'select',
        '#title' => $title,
        '#title_display' => 'invisible',
        '#value' => $value,
        '#attributes' => $element['#attributes'],
        '#options' => $options,
        '#required' => $element['#required'],
        '#error_no_message' => FALSE,
        '#empty_option' => $title,
      ];
    }

    // Allows custom callbacks to alter the element.
    if (!empty($element['#date_date_callbacks'])) {
      foreach ($element['#date_date_callbacks'] as $callback) {
        if (function_exists($callback)) {
          $callback($element, $form_state, $date);
        }
      }
    }

    return $element;
  }

}
