<?php

namespace Drupal\Tests\hijri\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\comment\Tests\CommentTestTrait;

/**
 * Browser test base class for Hijri functional tests.
 *
 * @group Hijri
 */
class HijriBlockDisplayTest extends BrowserTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['block', 'hijri', 'node'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->hijri_formatter = \Drupal::service('hijri.formatter');
    $this->assert = $this->assertSession();

    $this->drupalCreateContentType(
        [
          'type' => 'article',
          'name' => t('Article'),
        ]);

  }

  /**
   * {@inheritdoc}
   */
  public function testFullDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_display_block' => 'full',
    ];
    $this->submitForm($edit, 'Save configuration');
    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = 'stark';

    // Verify the blocks are listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name, ['query' => ['region' => 'content']]);
    $this->assert->pageTextContains('Hijri Date');

    // Define and place blocks.
    $settings_configurable = [
      'label' => 'Hijri Date',
      'id' => 'hijri_date_block',
      'theme' => $theme_name,
    ];
    $this->drupalPlaceBlock('hijri_date_block', $settings_configurable);

    $this->drupalGet('');
    $this->assert->pageTextContains($settings_configurable['label']);
    $this->assert->pageTextContainsOnce(t('@hijri on @gregorian', [
      '@hijri' => $this->hijri_formatter->format(time(), 'custom', 'l j F Y'),
      '@gregorian' => \Drupal::service('date.formatter')->format(time(), 'custom', 'F j, Y'),
    ]));

  }

  /**
   * {@inheritdoc}
   */
  public function testLongDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_display_block' => 'long',
    ];
    $this->submitForm($edit, 'Save configuration');
    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = 'stark';

    // Verify the blocks are listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name, ['query' => ['region' => 'content']]);
    $this->assert->pageTextContains('Hijri Date');

    // Define and place blocks.
    $settings_configurable = [
      'label' => 'Hijri Date',
      'id' => 'hijri_date_block',
      'theme' => $theme_name,
    ];
    $this->drupalPlaceBlock('hijri_date_block', $settings_configurable);

    $this->drupalGet('');
    $this->assert->pageTextContains($settings_configurable['label']);
    $this->assert->pageTextContainsOnce(t('@datetime', ['@datetime' => $this->hijri_formatter->format(time(), (String) \Drupal::config('hijri.config')->get('hijri_display_block'))]));

  }

  /**
   * {@inheritdoc}
   */
  public function testMediumDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_display_block' => 'medium',
    ];
    $this->submitForm($edit, 'Save configuration');
    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = 'stark';

    // Verify the blocks are listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name, ['query' => ['region' => 'content']]);
    $this->assert->pageTextContains('Hijri Date');

    // Define and place blocks.
    $settings_configurable = [
      'label' => 'Hijri Date',
      'id' => 'hijri_date_block',
      'theme' => $theme_name,
    ];
    $this->drupalPlaceBlock('hijri_date_block', $settings_configurable);

    $this->drupalGet('');
    $this->assert->pageTextContains($settings_configurable['label']);
    $this->assert->pageTextContainsOnce(t('@datetime', ['@datetime' => $this->hijri_formatter->format(time(), (String) \Drupal::config('hijri.config')->get('hijri_display_block'))]));

  }

  /**
   * {@inheritdoc}
   */
  public function testShortDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display_block' => 'short',
    ];
    $this->submitForm($edit, 'Save configuration');
    // Create user.
    $web_user = $this->drupalCreateUser(['administer blocks']);
    // Login the admin user.
    $this->drupalLogin($web_user);

    $theme_name = 'stark';

    // Verify the blocks are listed to be added.
    $this->drupalGet('/admin/structure/block/library/' . $theme_name, ['query' => ['region' => 'content']]);
    $this->assert->pageTextContains('Hijri Date');

    // Define and place blocks.
    $settings_configurable = [
      'label' => 'Hijri Date',
      'id' => 'hijri_date_block',
      'theme' => $theme_name,
    ];
    $this->drupalPlaceBlock('hijri_date_block', $settings_configurable);

    $this->drupalGet('');
    $this->assert->pageTextContains($settings_configurable['label']);
    $this->assert->pageTextContainsOnce(t('@datetime', ['@datetime' => $this->hijri_formatter->format(time(), (String) \Drupal::config('hijri.config')->get('hijri_display_block'))]));

  }

}
