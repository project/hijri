<?php

namespace Drupal\Tests\hijri\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\comment\Tests\CommentTestTrait;

/**
 * Browser test base class for Hijri functional tests.
 *
 * @group Hijri
 */
class HijriNodeDisplayTest extends BrowserTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'comment', 'hijri'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->hijri_formatter = \Drupal::service('hijri.formatter');
    $this->assert = $this->assertSession();
    $this->date_formatter = \Drupal::service('date.formatter');

    $this->drupalCreateContentType(
      [
        'type' => 'article',
        'name' => t('Article'),
      ]);
    // Create comment field on page.
    $this->addDefaultCommentField('node', 'article');

  }

  /**
   * {@inheritdoc}
   */
  public function testFullDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => "full",
      // 'hijri_comment_display' => 'full',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->account = $this->drupalCreateUser();
    $this->drupalLogin($this->account);
    $this->node = $this->drupalCreateNode(
      [
        'type' => 'article',
        'promote' => 1,
        'uid' => $this->account->id(),
      ]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assert->pageTextContainsOnce(t('Submitted by @username on @hijri on @gregorian', [
      '@username' => $this->account->getAccountName(),
      '@hijri' => $this->hijri_formatter->format($this->node->getCreatedTime(), 'custom', 'l j F Y'),
      '@gregorian' => \Drupal::service('date.formatter')->format($this->node->getCreatedTime(), 'custom', 'F j, Y'),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function testLongDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => "long",
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->account = $this->drupalCreateUser();
    $this->drupalLogin($this->account);
    $this->node = $this->drupalCreateNode(
      [
        'type' => 'article',
        'promote' => 1,
        'uid' => $this->account->id(),
      ]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assert->pageTextContainsOnce(t('Submitted by @username on @datetime',
       [
         '@username' => $this->account->getAccountName(),
         '@datetime' => $this->hijri_formatter->format($this->node->getCreatedTime(), (String) \Drupal::config('hijri.config')->get('hijri_display')),
       ]));
  }

  /**
   * {@inheritdoc}
   */
  public function testMediumDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => "medium",
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->account = $this->drupalCreateUser();
    $this->drupalLogin($this->account);

    $this->node = $this->drupalCreateNode(
      [
        'type' => 'article',
        'promote' => 1,
        'uid' => $this->account->id(),
      ]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assert->pageTextContainsOnce(t('Submitted by @username on @datetime',
     [
       '@username' => $this->account->getAccountName(),
       '@datetime' => $this->hijri_formatter->format($this->node->getCreatedTime(), (String) \Drupal::config('hijri.config')->get('hijri_display')),
     ]));
  }

  /**
   * {@inheritdoc}
   */
  public function testShortDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => "short",
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->account = $this->drupalCreateUser();
    $this->drupalLogin($this->account);
    $this->node = $this->drupalCreateNode(
      [
        'type' => 'article',
        'promote' => 1,
        'uid' => $this->account->id(),
      ]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assert->pageTextContainsOnce(t('Submitted by @username on @datetime',
    [
      '@username' => $this->account->getAccountName(),
      '@datetime' => $this->hijri_formatter->format($this->node->getCreatedTime(), (String) \Drupal::config('hijri.config')->get('hijri_display')),
    ]));

  }

  /**
   * {@inheritdoc}
   */
  public function testNoneDisplay() {
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => "none",
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->account = $this->drupalCreateUser();
    $this->drupalLogin($this->account);
    $this->node = $this->drupalCreateNode(
      [
        'type' => 'article',
        'promote' => 1,
        'uid' => $this->account->id(),
      ]);
    $this->drupalGet('node/' . $this->node->id());
    $this->assert->pageTextContainsOnce(t('Submitted by @username on @datetime',
    [
      '@username' => $this->account->getAccountName(),
      '@datetime' => $this->date_formatter->format($this->node->getCreatedTime()),
    ]));
  }

}
