<?php

namespace Drupal\Tests\hijri\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Browser test base class for Hijri functional tests.
 *
 * @group Hijri
 */
class HijriAdministrationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'hijri'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->hijri_formatter = \Drupal::service('hijri.formatter');
    $this->assert = $this->assertSession();

    $this->drupalCreateContentType(
      [
        'type' => 'article',
        'name' => t('Article'),
      ]);

  }

  /**
   * Tests configuring hijri.
   */
  public function testAdministration() {
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(403);
    $this->drupalLogin($this->createUser(['administer hijri']));
    $this->drupalGet('admin/config/regional/date-time/hijri');
    $this->assert->statusCodeEquals(200);
    $edit = [
      'hijri_correction_value' => 0,
      'hijri_types[]' => ['article'],
      'hijri_display' => 'short',
      'hijri_comment_display' => 'short',
      'hijri_display_block' => 'short',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assert->pageTextContainsOnce('The configuration options have been saved.');
    $this->drupalGet('admin/config/regional/date-time/hijri');
  }

}
