<?php

namespace Drupal\Tests\hijri\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the functionality of HijriFieldFormatter field formatter.
 *
 * @group field
 */
class HijriFieldFormatterTest extends BrowserTestBase {

  /**
   * An array of display options to pass to entity_get_display().
   *
   * @var array
   */
  protected $displayOptions;

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['hijri', 'datetime', 'entity_test', 'field_ui'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->hijri_formatter = \Drupal::service('hijri.formatter');

    $web_user = $this->drupalCreateUser([
      'access administration pages',
      'view test entity',
      'administer entity_test content',
      'administer entity_test fields',
      'administer entity_test display',
      'administer entity_test form display',
      'view the administration theme',
    ]);
    $this->drupalLogin($web_user);

    $field_name = 'field_hijri';
    $type = 'datetime';
    $widget_type = 'datetime_default';
    $formatter_type = 'hijri_field_formatter';

    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => $type,
    ]);
    $this->fieldStorage->save();
    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'entity_test',
      'required' => TRUE,
    ]);
    $this->field->save();

    EntityFormDisplay::load('entity_test.entity_test.default')
      ->setComponent($field_name, ['type' => $widget_type])
      ->save();

    $this->displayOptions = [
      'type' => $formatter_type,
      'label' => 'hidden',
    ];

    EntityViewDisplay::create([
      'targetEntityType' => $this->field->getTargetEntityTypeId(),
      'bundle' => $this->field->getTargetBundle(),
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent($field_name, $this->displayOptions)
      ->save();
  }

  /**
   * Tests the formatter settings.
   */
  public function testFullDisplay() {
    $format = 'full';
    $this->drupalGet('entity_test/structure/entity_test/display');

    $edit = [
      'fields[field_hijri][region]' => 'content',
      'fields[field_hijri][type]' => 'hijri_field_formatter',
    ];
    $this->submitForm($edit, 'Save');

    $this->submitForm([], 'field_hijri_settings_edit');
    $edit = [
      'fields[field_hijri][settings_edit_form][settings][hijri_formatter]' => $format,
    ];
    $this->submitForm($edit, 'Update');
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextContains($this->hijri_formatter->format(time(), 'custom', 'l j F Y') . " on " . \Drupal::service('date.formatter')->format(time(), 'custom', 'F j, Y'),);

  }

  /**
   * Tests the formatter settings.
   */
  public function testLongDisplay() {
    $format = 'long';
    $this->drupalGet('entity_test/structure/entity_test/display');

    $edit = [
      'fields[field_hijri][region]' => 'content',
      'fields[field_hijri][type]' => 'hijri_field_formatter',
    ];
    $this->submitForm($edit, 'Save');

    $this->submitForm([], 'field_hijri_settings_edit');
    $edit = [
      'fields[field_hijri][settings_edit_form][settings][hijri_formatter]' => $format,
    ];
    $this->submitForm($edit, 'Update');
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextContains($this->hijri_formatter->format(time(), $format));

  }

  /**
   * Tests the formatter settings.
   */
  public function testMediumDisplay() {
    $format = 'medium';
    $this->drupalGet('entity_test/structure/entity_test/display');

    $edit = [
      'fields[field_hijri][region]' => 'content',
      'fields[field_hijri][type]' => 'hijri_field_formatter',
    ];
    $this->submitForm($edit, 'Save');

    $this->submitForm([], 'field_hijri_settings_edit');
    $edit = [
      'fields[field_hijri][settings_edit_form][settings][hijri_formatter]' => $format,
    ];
    $this->submitForm($edit, 'Update');
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextContains($this->hijri_formatter->format(time(), $format));

  }

  /**
   * Tests the formatter settings.
   */
  public function testShortDisplay() {
    $format = 'short';
    $this->drupalGet('entity_test/structure/entity_test/display');

    $edit = [
      'fields[field_hijri][region]' => 'content',
      'fields[field_hijri][type]' => 'hijri_field_formatter',
    ];
    $this->submitForm($edit, 'Save');

    $this->submitForm([], 'field_hijri_settings_edit');
    $edit = [
      'fields[field_hijri][settings_edit_form][settings][hijri_formatter]' => $format,
    ];
    $this->submitForm($edit, 'Update');
    $this->submitForm([], 'Save');

    $this->assertSession()->pageTextContains($this->hijri_formatter->format(time(), $format));

  }

}
